package controle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Produtos;
/**
 * 
 * @author lucas
 * Classe Controle geral da Tabela Produtos
 */
public class ProdutosController {
	/**
	 * Método inserção de Informações para a tabela Produtos
	 * @param pro
	 * @return
	 */
	public boolean inserir(Produtos pro) {
		boolean resultado=false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="INSERT INTO Produtos(nome,descricao,preco,imagem,data) VALUES(?,?,?,?,?)";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, pro.getNome());
			ps.setString(2, pro.getDescricao());
			ps.setFloat(3,	pro.getPreco());
			ps.setString(4, pro.getImagem());
			ps.setString(5, pro.getData());
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de consulta de informações da tabela Produtos
	 * @return
	 */
	public ArrayList <Produtos> consultarProdutos(){
		ArrayList <Produtos> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Produtos");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList <Produtos>();
				while(rs.next()) {
				Produtos prod = new Produtos(rs.getInt("id"),rs.getString("nome"),rs.getString("descricao"),rs.getFloat("preco"),rs.getString("imagem"),rs.getString("data"));
					lista.add(prod);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	/**
	 * Método de consulta das ultimas informações contidas na tabela Produtos
	 * @return
	 */
	public ArrayList <Produtos> consultarUltimosProdutos(){
		ArrayList <Produtos> listau = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Produtos ORDER BY ID DESC limit 1; ");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				listau = new ArrayList <Produtos>();
				while(rs.next()) {
				Produtos prod = new Produtos(rs.getInt("id"),rs.getString("nome"),rs.getString("descricao"),rs.getFloat("preco"),rs.getString("imagem"),rs.getString("data"));
					listau.add(prod);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return listau;
	}
	/**
	 * Método de seleção especifica da tabela Produtos através do Id passado
	 * @param id
	 * @return
	 */
	public Produtos selecionarIdC(int id) {
		Produtos prod=null;
		try {
			Connection con= new Conexao().abrirConexao();
			String sql="SELECT * FROM Produtos WHERE id=?";
			PreparedStatement ps= con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs= ps.executeQuery();
			if(rs.next()) {
				prod = new Produtos (rs.getInt("id"),rs.getString("nome"),rs.getString("descricao"),rs.getFloat("preco"),rs.getString("imagem"),rs.getString("data"));
			}
		}catch(SQLException e) {
			e.getMessage();
		}
		return prod;
	}
	/**
	 * Método de Edição de informações da tabela Produtos
	 * @param prod
	 * @return
	 */
	public boolean editar(Produtos prod) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE Produtos SET nome=?,descricao=?,preco=?, imagem=?,data=? WHERE id=?");
			ps.setString(1,prod.getNome());
			ps.setString(2, prod.getDescricao());
			ps.setFloat(3, prod.getPreco());
			ps.setString(4, prod.getImagem());
			ps.setString(5,prod.getData());
			ps.setInt(6, prod.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar Produtos: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de remoção de dados da tabela Produtos
	 * @param id
	 * @return
	 */
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Produtos WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
}
