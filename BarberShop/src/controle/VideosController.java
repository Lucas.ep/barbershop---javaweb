package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Videos;
/**
 * 
 * @author lucas
 * Classe Controle geral da Tabela Videos
 */
public class VideosController {
	/**
	 * Método inserção de Informações para a tabela Videos
	 * @param vi
	 * @return
	 */
	public boolean inserir(Videos vi) {
		boolean resultado=false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="INSERT INTO Videos(video,titulo,descricao,data) VALUES(?,?,?,?)";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, vi.getVideo());
			ps.setString(2, vi.getTitulo());
			ps.setString(3, vi.getDescricao());
			ps.setString(4, vi.getData());
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de consulta de informações da tabela Videos
	 * @return
	 */
	public ArrayList <Videos> consultarVideos(){
		ArrayList <Videos> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Videos");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList <Videos>();
				while(rs.next()) {
				Videos tatu = new Videos(rs.getInt("id"),rs.getString("video"),rs.getString("titulo"),rs.getString("descricao"),rs.getString("data"));
					lista.add(tatu);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	/**
	 * Método de seleção especifica da tabela Videos através do Id passado
	 * @param id
	 * @return
	 */
	public Videos selecionarIdV(int id) {
		Videos video=null;
		try {
			Connection con= new Conexao().abrirConexao();
			String sql="SELECT * FROM Videos WHERE id=?";
			PreparedStatement ps= con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs= ps.executeQuery();
			if(rs.next()) {
				video = new Videos (rs.getInt("id"),rs.getString("video"),rs.getString("titulo"),rs.getString("descricao"),rs.getString("data"));
			}
		}catch(SQLException e) {
			e.getMessage();
		}
		return video;
	}
	/**
	 * Método de consulta das ultimas informações contidas na tabela Videos
	 * @return
	 */
	public ArrayList <Videos> consultarUltimosVideos(){
		ArrayList <Videos> listau = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Videos ORDER BY ID DESC limit 1; ");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				listau = new ArrayList <Videos>();
				while(rs.next()) {
				Videos tatu = new Videos(rs.getInt("id"),rs.getString("video"),rs.getString("titulo"),rs.getString("descricao"),rs.getString("data"));
					listau.add(tatu);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return listau;
	}
	/**
	 * Método de Edição de informações da tabela Videos
	 * @param video
	 * @return
	 */
	public boolean editar(Videos video) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE Videos SET video=?,titulo=?,descricao=?,data=? WHERE id=?");
			ps.setString(1, video.getVideo());
			ps.setString(2,video.getTitulo());
			ps.setString(3, video.getDescricao());
			ps.setString(4, video.getData());
			ps.setInt(5, video.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar Videos: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de remoção de dados da tabela Videos
	 * @param id
	 * @return
	 */
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Videos WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	
}
