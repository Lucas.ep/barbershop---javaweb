package controle;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.Usuario;
import java.util.ArrayList;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.catalina.manager.util.SessionUtils;

import com.sun.faces.renderkit.AttributeManager.Key;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * Classe Controle geral do Usuario Adm
 * @author lucas
 *
 */
public class UsuarioController {
	 /**
	  * Método de Verificação que irá retorna um valor boolean que será recebido na bean
	  * @param user
	  * @return
	  */
	public boolean verificar(Usuario user) {
		boolean resultado= false;
		try {
			Connection connect = new Conexao().abrirConexao();
			String sql="SELECT * FROM Usuario WHERE email=?";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, user.getEmail());
			ResultSet rs= ps.executeQuery();
			if(rs.next()){
				String key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDL3zlyBF/j6AfR\n" + 
				 		"9IwKAtZ6B5tq41b0L8dqm8FOycDNuHWGChg7CPNWrn44PVMsOQpy6SD532rqx/5A\n" + 
				 		"M36ieylS+jrADwk9PW258qTMZPMyx1DEJG2PiGfs+mpMD03lycRuGmLEKQeInIS9\n" + 
				 		"TClOmFFPeTnSkTeDkqX5y1A3TOH1FEXoGgKOpDWXZvGITlkl+kH4fwcoagLsIsek\n" + 
				 		"yK88byCw6r4NLH22MeQD9LxEXuihe0HqaMTu8CPSeBljVdEjkmYx6pp4UAfuqnn8\n" + 
				 		"zIuQFNUhuw1vJepj4pXIKsitoEXa918GuPetXjCtA1oAIzoUOF5tSpsPAoY44JJV\n" + 
				 		"es13rVhfAgMBAAECggEADAci8f2dFKqbT4FAg9SwB6oMOs2n0ydAeiMvT/EqPVjd\n" + 
				 		"IifVTyJTjPElhDbmAc1ptubXKbbLLYfYEbyYA4kFop4dujgI4QKPRzGwFFj+WigV\n" + 
				 		"NUU664VuMDaD7/HVNDHns2E+I3mSNraZRDvKkhb9cRVjWm9z2YDc5vReSqzwBc/t\n" + 
				 		"4SG7j2cBOicOd+dOZh7DfsMWORERpcdQjazQf1ArdXV2GwuBPRdz1BfgvrZmNj9o\n" + 
				 		"6/WjVQV+MtMnX7HAmsw2s8jUCYO3lfx7I0/m/EIOFLfbuAgP4y+bVhDyKlQnQLOg\n" + 
				 		"VrZmiYqB05JcqGsw51Df7sK8kKzBrTi+FOtANb7dpQKBgQDyPsQEBFe+tVKDUMGo\n" + 
				 		"+LytXbheENbcq4t1C7c+ZmK7wzDwTdLOhdw8QcDyif3bm2NZcBNqZSYegWoFLn+2\n" + 
				 		"H/SzntAIvAGzzE8Z86FeEKQu4PnfUuxpNa6oDaTdLgBBAJoRTD8wzsCMkwET8sGp\n" + 
				 		"6ge6Whzz4r83NO8mz+q/R94bYwKBgQDXcqwe13Y78i7fkhs3vv2UxLbnUbAJKcjK\n" + 
				 		"3l2JlxJmvamtZ1Ew0jRsXYzAYvCIS+/GFy4r2YzlBLEbd2bi7l/sgh50UNSpXaSx\n" + 
				 		"uCARYZOm5Z70LxvhkMGmLXHPV1kzxa6Sy72XmYXhaBc/Ns4orbJ+sMeJOAbm72Po\n" + 
				 		"4RNfuWvl1QKBgDsPRmbcUDA0sNtHExAJJKb31H1KibffMu7kXlaeS7APVJ0hvCWR\n" + 
				 		"yTH/rfTz46po5f3mLzWfV33Ue26r+YMDo3svWvTmMVwOkbJ4DX2LfRvYydLCutSj\n" + 
				 		"u+NJAErUbkdqyCUze6yAm70qEfc1FjZA0oWCdtCXFZt2EmBaDJd6BBKVAoGBAME0\n" + 
				 		"iZvi1pmtdlFxwcy9DsShn/BS9g1RlkovHSys+IiAHzBszYd9iht/zSAd2dwwVOaM\n" + 
				 		"lRAnuM0L5xNdgTuSTx1WFp9yeTMk0fO5zbAok/OASYpq0JL4cGBosn4gs9LUvNfR\n" + 
				 		"s8TGnSPlZ6t9p2UdV0t7loS8ZJwmI6+MYAZgzpy9AoGBAIsrLcu8B0O0SWbtPH/C\n" + 
				 		"jtq4YFt79/aSbCr6JtRQE6Yvs2RaqxcSe7coEugz9weMl3oOqVaYe5zIjUQBxdkh\n" + 
				 		"+xulA5lcLblZMyMCplo3debzEZ+tIN736r5bmQjEzq+PeIyQgtleoXwjfpEHyQMD\n" + 
				 		"eszFTW8/Jff2F4lVO/5J8xR6";
				Crypt crip= new Crypt();
				String result= crip.decrypt(rs.getBytes("senha"), key);
				if(user.getSenha().equals(result)) {
					resultado=true;
				}
			}
			new Conexao().fecharConexao(connect);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de inserção de novas informações de um novo Usuario com segurança criptografica de senhas
	 * @param user
	 * @return
	 */
	public boolean cadastrar(Usuario user) {
		boolean resultado= false;
		try {	
			String key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDL3zlyBF/j6AfR\n" + 
			 		"9IwKAtZ6B5tq41b0L8dqm8FOycDNuHWGChg7CPNWrn44PVMsOQpy6SD532rqx/5A\n" + 
			 		"M36ieylS+jrADwk9PW258qTMZPMyx1DEJG2PiGfs+mpMD03lycRuGmLEKQeInIS9\n" + 
			 		"TClOmFFPeTnSkTeDkqX5y1A3TOH1FEXoGgKOpDWXZvGITlkl+kH4fwcoagLsIsek\n" + 
			 		"yK88byCw6r4NLH22MeQD9LxEXuihe0HqaMTu8CPSeBljVdEjkmYx6pp4UAfuqnn8\n" + 
			 		"zIuQFNUhuw1vJepj4pXIKsitoEXa918GuPetXjCtA1oAIzoUOF5tSpsPAoY44JJV\n" + 
			 		"es13rVhfAgMBAAECggEADAci8f2dFKqbT4FAg9SwB6oMOs2n0ydAeiMvT/EqPVjd\n" + 
			 		"IifVTyJTjPElhDbmAc1ptubXKbbLLYfYEbyYA4kFop4dujgI4QKPRzGwFFj+WigV\n" + 
			 		"NUU664VuMDaD7/HVNDHns2E+I3mSNraZRDvKkhb9cRVjWm9z2YDc5vReSqzwBc/t\n" + 
			 		"4SG7j2cBOicOd+dOZh7DfsMWORERpcdQjazQf1ArdXV2GwuBPRdz1BfgvrZmNj9o\n" + 
			 		"6/WjVQV+MtMnX7HAmsw2s8jUCYO3lfx7I0/m/EIOFLfbuAgP4y+bVhDyKlQnQLOg\n" + 
			 		"VrZmiYqB05JcqGsw51Df7sK8kKzBrTi+FOtANb7dpQKBgQDyPsQEBFe+tVKDUMGo\n" + 
			 		"+LytXbheENbcq4t1C7c+ZmK7wzDwTdLOhdw8QcDyif3bm2NZcBNqZSYegWoFLn+2\n" + 
			 		"H/SzntAIvAGzzE8Z86FeEKQu4PnfUuxpNa6oDaTdLgBBAJoRTD8wzsCMkwET8sGp\n" + 
			 		"6ge6Whzz4r83NO8mz+q/R94bYwKBgQDXcqwe13Y78i7fkhs3vv2UxLbnUbAJKcjK\n" + 
			 		"3l2JlxJmvamtZ1Ew0jRsXYzAYvCIS+/GFy4r2YzlBLEbd2bi7l/sgh50UNSpXaSx\n" + 
			 		"uCARYZOm5Z70LxvhkMGmLXHPV1kzxa6Sy72XmYXhaBc/Ns4orbJ+sMeJOAbm72Po\n" + 
			 		"4RNfuWvl1QKBgDsPRmbcUDA0sNtHExAJJKb31H1KibffMu7kXlaeS7APVJ0hvCWR\n" + 
			 		"yTH/rfTz46po5f3mLzWfV33Ue26r+YMDo3svWvTmMVwOkbJ4DX2LfRvYydLCutSj\n" + 
			 		"u+NJAErUbkdqyCUze6yAm70qEfc1FjZA0oWCdtCXFZt2EmBaDJd6BBKVAoGBAME0\n" + 
			 		"iZvi1pmtdlFxwcy9DsShn/BS9g1RlkovHSys+IiAHzBszYd9iht/zSAd2dwwVOaM\n" + 
			 		"lRAnuM0L5xNdgTuSTx1WFp9yeTMk0fO5zbAok/OASYpq0JL4cGBosn4gs9LUvNfR\n" + 
			 		"s8TGnSPlZ6t9p2UdV0t7loS8ZJwmI6+MYAZgzpy9AoGBAIsrLcu8B0O0SWbtPH/C\n" + 
			 		"jtq4YFt79/aSbCr6JtRQE6Yvs2RaqxcSe7coEugz9weMl3oOqVaYe5zIjUQBxdkh\n" + 
			 		"+xulA5lcLblZMyMCplo3debzEZ+tIN736r5bmQjEzq+PeIyQgtleoXwjfpEHyQMD\n" + 
			 		"eszFTW8/Jff2F4lVO/5J8xR6";
			Crypt crip= new Crypt();
			byte[] resul;
			String senha= user.getSenha();
			resul= crip.encrypt(senha, key);
			Connection connect = new Conexao().abrirConexao();
			String sql="INSERT INTO Usuario(nome,email,senha) VALUES(?,?,?);";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, user.getNome());
			ps.setString(2, user.getEmail());
			ps.setBytes(3, resul);
			if(!ps.execute()){
			     resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(Exception e) {
			e.getMessage();
		}
		return resultado;
	}
}
