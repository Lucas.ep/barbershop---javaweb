package controle;
import java.awt.Image;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Reader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.Novidade;
import java.util.ArrayList;
import java.util.Date;

import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.Part;

import com.mysql.jdbc.Blob;
/**
 * 
 * @author lucas
 * Classe Controle geral da Tabela Novidades
 */
public class NovidadeController {
	/**
	 * Método inserção de Informações para a tabela Novidades
	 * @param nov
	 * @return
	 */
	public boolean inserir(Novidade nov) {
		boolean resultado=false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="INSERT INTO Novidades(imagem,titulo,descricao,data) VALUES(?,?,?,?)";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, nov.getImagem());
			ps.setString(2, nov.getTitulo());
			ps.setString(3, nov.getDescricao());
			ps.setString(4,	nov.getData());
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de consulta de informações da tabela Novidades
	 * @return
	 */
	public ArrayList <Novidade> consultarNovidades(){
		ArrayList <Novidade> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Novidades");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList <Novidade>();
				while(rs.next()) {
					
				Novidade novi = new Novidade(rs.getInt("id"),rs.getString("titulo"),rs.getString("imagem"),rs.getString("descricao"),rs.getString("data"));
					lista.add(novi);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	/**
	 * Método de consulta das ultimas informações contidas na tabela Novidades
	 * @return
	 */
	public ArrayList <Novidade> consultarUltimaNovidades(){
		ArrayList <Novidade> listau = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Novidades ORDER BY ID DESC limit 1;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				listau = new ArrayList <Novidade>();
				while(rs.next()) {
				Novidade novi = new Novidade(rs.getInt("id"),rs.getString("titulo"),rs.getString("imagem"),rs.getString("descricao"),rs.getString("data"));
					listau.add(novi);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return listau;
	}
	/**
	 * Método de seleção especifica da tabela Novidades através do Id passado
	 * @param id
	 * @return
	 */
	public Novidade selecionarIdN(int id) {
		Novidade novi=null;
		try {
			Connection con= new Conexao().abrirConexao();
			String sql="SELECT * FROM Novidades WHERE id=?";
			PreparedStatement ps= con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs= ps.executeQuery();
			if(rs.next()) {
				novi = new Novidade (rs.getInt("id"),rs.getString("titulo"),rs.getString("imagem"),rs.getString("descricao"),rs.getString("data"));
			}
		}catch(SQLException e) {
			e.getMessage();
		}
		return novi;
	}
	/**
	 * Método de seleção de informações da Tabela Novidades
	 * @param titulo
	 * @return
	 */
	public ArrayList<Novidade> pesquisa(String titulo) {
		ArrayList<Novidade> lista=null;
		try {
			Connection con= new Conexao().abrirConexao();
			String sql="SELECT * FROM Novidades WHERE titulo like ?";
			PreparedStatement ps= con.prepareStatement(sql);
			ps.setString(1, "%"+titulo+"%");
			ResultSet rs= ps.executeQuery();
			if(rs!=null) {
				lista= new ArrayList<Novidade>();
				while(rs.next()) {
					Novidade novi = new Novidade (rs.getInt("id"),rs.getString("titulo"),rs.getString("imagem"),rs.getString("descricao"),rs.getString("data"));
					lista.add(novi);
				}
			}
		}catch(SQLException e) {
			e.getMessage();
		}
		return lista;
	}
	/**
	 * Método de Edição de informações da tabela Novidades
	 * @param novi
	 * @return
	 */
	public boolean editar(Novidade novi) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE Novidades SET titulo=?,descricao=?, imagem=?,data=? WHERE id=?");
			ps.setString(1,novi.getTitulo());
			ps.setString(2, novi.getDescricao());
			ps.setString(3,novi.getImagem());
			ps.setString(4, novi.getData());
			ps.setInt(5, novi.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar novidade: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de remoção de dados da tabela Novidades
	 * @param id
	 * @return
	 */
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Novidades WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}



	
}
