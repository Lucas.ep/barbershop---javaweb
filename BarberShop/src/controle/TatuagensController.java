package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Tatuagens;
/**
 * 
 * @author lucas
 * Classe Controle geral da Tabela Tatuagens
 */
public class TatuagensController {
	/**
	 * Método inserção de Informações para a tabela Tatuagens
	 * @param tatu
	 * @return
	 */
	public boolean inserir(Tatuagens tatu) {
		boolean resultado=false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="INSERT INTO Tatuagens(imagem,data) VALUES(?,?)";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, tatu.getImagem());
			ps.setString(2, tatu.getData());
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de consulta de informações da tabela Tatuagens
	 * @return
	 */
	public ArrayList <Tatuagens> consultarTatuagens(){
		ArrayList <Tatuagens> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Tatuagens");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList <Tatuagens>();
				while(rs.next()) {
				Tatuagens tatu = new Tatuagens(rs.getInt("id"),rs.getString("imagem"),rs.getString("data"));
					lista.add(tatu);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	/**
	 * Método de seleção especifica da tabela Tatuagens através do Id passado
	 * @param id
	 * @return
	 */
	public Tatuagens selecionarIdT(int id) {
		Tatuagens tatu=null;
		try {
			Connection con= new Conexao().abrirConexao();
			String sql="SELECT * FROM Tatuagens WHERE id=?";
			PreparedStatement ps= con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs= ps.executeQuery();
			if(rs.next()) {
				tatu = new Tatuagens(rs.getInt("id"),rs.getString("imagem"),rs.getString("data"));
			}
		}catch(SQLException e) {
			e.getMessage();
		}
		return tatu;
	}
	/**
	 * Método de consulta das ultimas informações contidas na tabela Tatuagens
	 * @return
	 */
	public ArrayList <Tatuagens> consultarUltimasTatuagens(){
		ArrayList <Tatuagens> listau = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Tatuagens ORDER BY ID DESC limit 1;");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				listau = new ArrayList <Tatuagens>();
				while(rs.next()) {
				Tatuagens tatu = new Tatuagens(rs.getInt("id"),rs.getString("imagem"),rs.getString("data"));
					listau.add(tatu);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return listau;
	}
	/**
	 * Método de Edição de informações da tabela Tatuagens
	 * @param tatu
	 * @return
	 */
	public boolean editar(Tatuagens tatu) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE Tatuagens SET imagem=?,data=? WHERE id=?");
			System.out.print(tatu.getId());
			ps.setString(1, tatu.getImagem());
			ps.setString(2, tatu.getData());
			ps.setInt(2, tatu.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar Tatuagens: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de remoção de dados da tabela Tatuagens
	 * @param id
	 * @return
	 */
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Tatuagens WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
	
}
