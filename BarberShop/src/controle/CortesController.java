package controle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Cortes;
import modelo.Cortes;
/**
 * 
 * @author lucas
 * Classe Controle geral da Tabela Cortes
 */
public class CortesController {
	/**
	 * Método inserção de Informações para a tabela Cortes
	 * @param cor
	 * @return
	 */
	public boolean inserir(Cortes cor) {
		boolean resultado=false;
		try {
			Connection connect= new Conexao().abrirConexao();
			String sql="INSERT INTO Cortes(nome,descricao,preco,imagem,data) VALUES(?,?,?,?,?)";
			PreparedStatement ps= connect.prepareStatement(sql);
			ps.setString(1, cor.getNome());
			ps.setString(2, cor.getDescricao());
			ps.setFloat(3,	cor.getPreco());
			ps.setString(4, cor.getImagem());
			ps.setString(5, cor.getData());
			if(!ps.execute()) {
				resultado=true;
			}
			new Conexao().fecharConexao(connect);
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de consulta de informações da tabela Cortes
	 * @return
	 */
	public ArrayList <Cortes> consultarCortes(){
		ArrayList <Cortes> lista = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Cortes");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				lista = new ArrayList <Cortes>();
				while(rs.next()) {
				Cortes cort = new Cortes(rs.getInt("id"),rs.getString("nome"),rs.getString("descricao"),rs.getFloat("preco"),rs.getString("imagem"),rs.getString("data"));
					lista.add(cort);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return lista;
	}
	/**
	 * Método de consulta das ultimas informações contidas na tabela Cortes
	 * @return
	 */
	public ArrayList <Cortes> consultarUltimosCortes(){
		ArrayList <Cortes> listau = null;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM Cortes ORDER BY ID DESC limit 1; ");
			ResultSet rs = ps.executeQuery();
			if(rs != null) {
				listau = new ArrayList <Cortes>();
				while(rs.next()) {
				Cortes cort = new Cortes(rs.getInt("id"),rs.getString("nome"),rs.getString("descricao"),rs.getFloat("preco"),rs.getString("imagem"),rs.getString("data"));
					listau.add(cort);
				}
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return listau;
	}
	/**
	 * Método de seleção especifica da tabela Cortes através do Id passado
	 * @param id
	 * @return
	 */
	public Cortes selecionarIdC(int id) {
		Cortes cort=null;
		try {
			Connection con= new Conexao().abrirConexao();
			String sql="SELECT * FROM Cortes WHERE id=?";
			PreparedStatement ps= con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs= ps.executeQuery();
			if(rs.next()) {
				cort = new Cortes (rs.getInt("id"),rs.getString("nome"),rs.getString("descricao"),rs.getFloat("preco"),rs.getString("imagem"),rs.getString("data"));
			}
		}catch(SQLException e) {
			e.getMessage();
		}
		return cort;
	}
	/**
	 * Método de Edição de informações da tabela Cortes
	 * @param cort
	 * @return
	 */
	public boolean editar(Cortes cort) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("UPDATE Cortes SET nome=?,descricao=?,preco=?, imagem=?,data=? WHERE id=?");
			ps.setString(1,cort.getNome());
			ps.setString(2, cort.getDescricao());
			ps.setFloat(3, cort.getPreco());
			ps.setString(4, cort.getImagem());
			ps.setString(5, cort.getData());
			ps.setInt(6, cort.getId());
			if(!ps.execute()) {
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro ao editar Cortes: " + e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro: "+ e.getMessage());
		}
		return resultado;
	}
	/**
	 * Método de remoção de dados da tabela Cortes
	 * @param id
	 * @return
	 */
	public boolean remover(int id) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			PreparedStatement ps = con.prepareStatement("DELETE FROM Cortes WHERE id=?");
			ps.setInt(1, id);
			if(ps.execute()){
				resultado = true;
			}
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
		return resultado;
	}
}
