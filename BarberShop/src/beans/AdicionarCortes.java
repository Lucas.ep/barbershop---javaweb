package beans;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import modelo.Cortes;
import modelo.Novidade;
import modelo.Tatuagens;
import controle.CortesController;
import controle.NovidadeController;
import controle.TatuagensController;
/**
 * 
 * @author lucas
 *
 */
@ManagedBean(name="AdicionarCortes")
public class AdicionarCortes {
	private int id;
	private String nome,descricao,diretorio,data;
	private float preco;
	private Part imagem;
	
	/**
	 * Método de acesso para o retorno do valor da data
	 * @return
	 */
	public String getData() {
		return data;
	}
	/**
	 * Método de modificação do valor da data
	 * @param data
	 */
	public void setData(String data) {
		this.data = data;
	}
	/**
	 * Método de acesso para o retorno do valor do id
	 * @return
	 */
	public int getId() {
		return id;
	}
	/**
	 * Método de modificação do valor do id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Método de acesso para o retorno da Imagem
	 * @return
	 */
	public Part getImagem() {
		return imagem;
	}
	/**
	 * Método de modificação do valor da Imagem
	 * @param imagem
	 */
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	}
	/**
	 * Método de acesso para o retorno do valor do preço
	 * @return
	 */
	public float getPreco() {
		return preco;
	}
	/**
	 * Método de modificação do valor do preço
	 * @param preco
	 */
	public void setPreco(float preco) {
		this.preco = preco;
	}
	/**
	 * Método de acesso para o retorno do valor do nome
	 * @return
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * Método de modificação do valor do nome
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	/**
	 * Método de acesso para o retorno do valor da descrição
	 * @return
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Método de modificação do valor da descrição
	 * @param descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * Método de acesso para o retorno do valor do link de diretorio
	 * @return
	 */
	public String getDiretorio() {
		return diretorio;
	}
	/**
	 * Método de modificação do valor do link de diretorio
	 * @param diretorio
	 */
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}

	/**
	 * Array das informações contidas na tabela Cortes
	 */
	ArrayList<Cortes> lista = new CortesController().consultarCortes();
	public ArrayList <Cortes> getLista() {
		return lista;
	}
	public void setLista(ArrayList <Cortes> lista) {
		this.lista = lista;
	}
	public Cortes retornarItem(int id) {
		return lista.get(id);
	}
	/**
	 * Array das ultimas informações contidas na tabela Cortes
	 */
	ArrayList<Cortes> listau = new CortesController().consultarUltimosCortes();
	public ArrayList <Cortes> getListau() {
		return listau;
	}
	public void setListau(ArrayList <Cortes> listau) {
		this.listau = listau;
	}
	/**
	 * Método de Inserção de informações na tabela Cortes
	 * @return
	 * @throws IOException
	 */
	public String adicionarCortes() throws IOException {
		try(InputStream entrada= imagem.getInputStream()) {
			
			String pasta= "/home/lucas/eclipse-workspace/BarberShop/WebContent/resources/imagens/";
			String nome= imagem.getSubmittedFileName();
			Files.copy(entrada, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
			String a= pasta.substring(51,70);
			String caminho= ""+a+""+nome+"";
			Date dAtual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String dataAtual = sdf.format(dAtual);
			this.setData(dataAtual);
			Cortes cor= new Cortes(this.getId(), this.getNome(),this.getDescricao(),this.getPreco(),caminho,this.getData());
			if(new CortesController().inserir(cor)) {
				return "home.xhtml?faces-redirect=true;";
			}else {
				return "index.xhtml?faces-redirect=true;";
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Método para o preenchimento de dados na pagina de Edição de Cortes
	 * @param id
	 */
	public void carregarId(int id) {
		Cortes cort= new CortesController().selecionarIdC(id);
		this.setId(cort.getId());
		this.setNome(cort.getNome());
		this.setDescricao(cort.getDescricao());
		this.setPreco(cort.getPreco());
		this.setDiretorio(cort.getImagem());
		this.setData(cort.getData());
	}
	
	/**
	 * Método para a alteração das informações especificas de um bloco da tabela Cortes
	 * @return
	 */
	public String editar() {
		try(InputStream entrada= imagem.getInputStream()) {
			String pasta= "/home/lucas/eclipse-workspace/BarberShop/WebContent/resources/imagens/";
			String nome= imagem.getSubmittedFileName();
			Files.copy(entrada, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
			String a= pasta.substring(51,70);
			String caminho= ""+a+""+nome+"";
			Date dAtual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String dataAtual = sdf.format(dAtual);
			this.setData(dataAtual);
			Cortes cor= new Cortes(this.getId(), this.getNome(),this.getDescricao(),this.getPreco(),caminho,this.getData());
			if(new CortesController().editar(cor)) {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			    String url = ec.getRequestContextPath() + "/faces/home.xhtml";
			    ec.redirect(url);
			}else {
				return "index.xhtml?faces-redirect=true;";
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Método para deletar informações especificas da tabela Cortes
	 * @param id
	 * @return
	 */
	public String deletar(int id) {
		new CortesController().remover(id);
		return "home.xhtml?faces-redirect=true;";
	}
}
