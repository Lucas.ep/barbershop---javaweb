package beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

import controle.ProdutosController;
import controle.TatuagensController;
import modelo.Produtos;
import modelo.Tatuagens;
/**
 * 
 * @author lucas
 *
 */
@ManagedBean(name="AdicionarTatoos")

public class AdicionarTatoos {
	private int id;
	private Part imagem;
	private String diretorio,mensagem,data;
	
	/**
	 * Método de acesso para o retorno do valor da data
	 * @return
	 */
	public String getData() {
		return data;
	}
	/**
	 * Método de modificação do valor da data
	 * @param data
	 */
	public void setData(String data) {
		this.data = data;
	}
	/**
	 * Método de acesso para o retorno do valor da mensagem para o Adm
	 * @return
	 */
	public String getMensagem() {
		return mensagem;
	}
	/**
	 * Método de modificação do valor da mensagem
	 * @param mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	/**
	 * Método de acesso para o retorno do valor do id
	 * @return
	 */
	public int getId() {
		return id;
	}
	/**
	 * Método de modificação do valor do id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Método de acesso para o retorno da Imagem
	 * @return
	 */
	public Part getImagem() {
		return imagem;
	}
	/**
	 * Método de modificação do valor da Imagem
	 * @param imagem
	 */
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	}
	/**
	 * Método de acesso para o retorno do valor do link de diretorio
	 * @return
	 */
	public String getDiretorio() {
		return diretorio;
	}
	/**
	 * Método de modificação do valor do link de diretorio
	 * @param diretorio
	 */
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}
	/**
	 * Método de Inserção de informações na tabela Tatuagens
	 * @return
	 * @throws IOException
	 */
	public String adicionarTatuagens(){
			try(InputStream entrada= imagem.getInputStream()) {
				
				String pasta= "/home/lucas/eclipse-workspace/BarberShop/WebContent/resources/imagens/";
				String nome= imagem.getSubmittedFileName();
				Files.copy(entrada, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
				String a= pasta.substring(51,70);
				String caminho= ""+a+""+nome+"";
				Date dAtual = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				String dataAtual = sdf.format(dAtual);
				this.setData(dataAtual);
				Tatuagens tatu = new Tatuagens(this.getId(), caminho,this.getData());
				if(new TatuagensController().inserir(tatu)) {
					ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
				    String url = ec.getRequestContextPath() + "/faces/home.xhtml";
				    ec.redirect(url);			
				}else {				
					this.setMensagem("<script>UIkit.modal.alert('Tatuagem não adicionada!').then(function() {\n" + 
							"    console.log('Confirmed.')\n" + 
							"});</script>");
					return null;
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			return null;
	}
	/**
	 * Array das informações contidas na tabela Tatuagens
	 */
	ArrayList<Tatuagens> lista = new TatuagensController().consultarTatuagens();
	public ArrayList <Tatuagens> getLista() {
		return lista;
	}
	public void setLista(ArrayList <Tatuagens> lista) {
		this.lista = lista;
	}
	public Tatuagens retornarItem(int id) {
		return lista.get(id);
	}
	/**
	 * Array das ultimas informações contidas na tabela Tatuagens
	 */
	ArrayList<Tatuagens> listau = new TatuagensController().consultarUltimasTatuagens();
	public ArrayList <Tatuagens> getListau() {
		return listau;
	}
	public void setListau(ArrayList <Tatuagens> listau) {
		this.listau = listau;
	}
	/**
	 * Método para o preenchimento de dados na pagina de Edição de Tatuagens
	 * @param id
	 */
	public void carregarId(int id) {
		Tatuagens tatu = new TatuagensController().selecionarIdT(id);
		this.setId(tatu.getId());
		this.setDiretorio(tatu.getImagem());
	}
	/**
	 * Método para a alteração das informações especificas de um bloco da tabela Tatuagens
	 * @return
	 */
	public String editar() {
		try(InputStream entrada= imagem.getInputStream()) {
			String pasta= "/home/lucas/eclipse-workspace/BarberShop/WebContent/resources/imagens/";
			String nome= imagem.getSubmittedFileName();
			Files.copy(entrada, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
			String a= pasta.substring(51,70);
			String caminho= ""+a+""+nome+"";
			Date dAtual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String dataAtual = sdf.format(dAtual);
			this.setData(dataAtual);
			Tatuagens tatu= new Tatuagens(this.getId(),caminho,this.getData());
			if(new TatuagensController().editar(tatu)) {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			    String url = ec.getRequestContextPath() + "/faces/home.xhtml";
			    ec.redirect(url);			
			}else {				
				this.setMensagem("<script>UIkit.modal.alert('Tatuagem não editada!').then(function() {\n" + 
						"    console.log('Confirmed.')\n" + 
						"});</script>");
				return null;
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	/**
	 * Método para deletar informações especificas da tabela Tatuagens
	 * @param id
	 * @return
	 */
	public String deletar(int id) {
		new TatuagensController().remover(id);
		return "home.xhtml?faces-redirect=true;";
	}
	
}