package beans;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.mysql.fabric.xmlrpc.base.Data;
import com.mysql.jdbc.util.Base64Decoder;

import modelo.Usuario;
import controle.UsuarioController;
@SessionScoped
/**
 * Bean de uso de métodos para o gerenciamento do Usuario no sistema
 * @author lucas
 *
 */
@ManagedBean(name="verificarUser")
public class verificarUser {
		
		private int id;
		private String nome,email,senha,mensagem;
		public boolean logado=false;
		public boolean mostrar=false;
		
		/**
		 * Método boolean que irá retornar o status do Usuario no sistema
		 * @return
		 */
		public boolean isMostrar() {
			return mostrar;
		}
		/**
		 * Método de modificação do status do Usuario 
		 * @param mostrar
		 */
		public void setMostrar(boolean mostrar) {
			this.mostrar = mostrar;
		}
		/**
		 * Método de acesso com retorno do valor do Id
		 * @return
		 */
		public int getId() {
			return id;
		}
		/**
		 * Método de modificação do valor do Id
		 * @param id
		 */
		public void setId(int id) {
			this.id = id;
		}
		/**
		 * Método de acesso com retorno do valor do Nome do Usuario
		 * @return
		 */
		public String getNome() {
			return nome;
		}
		/**
		 * Método de modificação do valor do Nome do Usuario
		 * @param nome
		 */
		public void setNome(String nome) {
			this.nome = nome;
		}
		/**
		 * Método de acesso com retorno do email do Usuario
		 * @return
		 */
		public String getEmail() {
			return email;
		}
		/**
		 * Método modificação do valor do email do Usuario
		 * @param email
		 */
		public void setEmail(String email) {
			this.email = email;
		}
		/**
		 * Método de acesso com retorno do valor da senha do Usuario
		 * @return
		 */
		public String getSenha() {
			return senha;
		}
		/**
		 * Método de modificação do valor da senha do Usuario
		 * @param senha
		 */
		public void setSenha(String senha) {
			this.senha = senha;
		}
		/**
		 * Método de acesso com retorno do valor da mensagem de verificação
		 * @return
		 */
		public String getMensagem() {
			return mensagem;
		}
		/**
		 * Método de modificação da mensagem de verificação
		 * @param mensagem
		 */
		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
		/**
		 * Método de verificação de informações do Usuario com as do Banco de Dados
		 * @return
		 */
		public String verificar() {
			Usuario user = new Usuario(this.getNome(),this.getEmail(),this.getSenha());
			if(new UsuarioController().verificar(user)) {
				logado=true;
				mostrar=true;
				return "home.xhtml?faces-redirect=true";
			}else {
				this.setMensagem("<script>UIkit.modal.alert('Email ou Senha incorretos!').then(function() {\n" + 
						"    console.log('Confirmed.')\n" + 
						"});</script>");
				return null;
			}
		}
		/**
		 * Métdo de Inserção de novas informações para um novo Usuario
		 * @throws NoSuchAlgorithmException
		 * @throws NoSuchPaddingException
		 * @throws IllegalBlockSizeException
		 * @throws BadPaddingException
		 * @throws InvalidKeyException
		 * @throws UnsupportedEncodingException
		 * @throws NoSuchProviderException
		 * @throws InvalidAlgorithmParameterException
		 */
		public void cadastrar() throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, UnsupportedEncodingException, NoSuchProviderException, InvalidAlgorithmParameterException{
			Usuario user= new Usuario (this.getNome(),this.getEmail(),this.getSenha());
			if(new UsuarioController().cadastrar(user)) {
				this.setMensagem("<script>UIkit.modal.alert('Cadastro feito com sucesso!').then(function() {\n" + 
						"    console.log('Confirmed.')\n" + 
						"});</script>");
			}else {
				this.setMensagem("<script>UIkit.modal.alert('Cadastro deu erro!').then(function() {\n" + 
						"    console.log('Confirmed.')\n" + 
						"});</script>");
			}
		}

		


}