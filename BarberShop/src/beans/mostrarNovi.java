package beans;
import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import controle.NovidadeController;
import modelo.Novidade;

@ManagedBean(name="mostrarNovi")
public class mostrarNovi {
	private String pesquisa;
	
	public String getPesquisa() {
		return pesquisa;
	}
	public void setPesquisa(String pesquisa) {
		this.pesquisa = pesquisa;
	}

	ArrayList<Novidade> lista = new NovidadeController().consultarNovidades();
	public ArrayList <Novidade> getLista() {
		return lista;
	}
	public void setLista(ArrayList <Novidade> lista) {
		this.lista = lista;
	}
	public Novidade retornarItem(int id) {
		return lista.get(id);
	}
	
	ArrayList<Novidade> listau = new NovidadeController().consultarUltimaNovidades();
	public ArrayList <Novidade> getListau() {
		return listau;
	}
	public void setListau(ArrayList <Novidade> listau) {
		this.listau = listau;
	}
	
	public void pesquisaa() {
		ArrayList <Novidade> lista=null;
		if(pesquisa==null) {
			lista= new NovidadeController().consultarNovidades();
			this.setLista(lista);
		}else {
			lista= new NovidadeController().pesquisa(pesquisa);
		}
		this.setLista(lista);
	}


}



