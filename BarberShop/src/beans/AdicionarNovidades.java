package beans;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.text.SimpleDateFormat;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;
import modelo.Novidade;
import controle.NovidadeController;
@ManagedBean(name="AdicionarNovidade")
/**
 * 
 * @author lucas
 *
 * @param <ImagemBase64>
 */
public class AdicionarNovidades<ImagemBase64> {
	
	private int id;
	private Part imagem;
	private String titulo,descricao,diretorio,mensagem;
	private String data;
	
	/**
	 * Método de acesso para o retorno do valor da data
	 * @return
	 */
	public String getData() {
		return data;
	}
	/**
	 * Método de modificação do valor da data
	 * @param data
	 */
	public void setData(String data) {
		this.data = data;
	}
	/**
	 * Método de acesso para o retorno do valor do id
	 * @return
	 */
	public int getId() {
		return id;
	}
	/**
	 * Método de modificação do valor do id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Método de acesso para o retorno da Imagem
	 * @return
	 */
	public Part getImagem() {
		return imagem;
	}
	/**
	 * Método de modificação do valor da Imagem
	 * @param imagem
	 */
	public void setImagem(Part imagem) {
		this.imagem = imagem;
	}
	/**
	 * Método de acesso para o retorno do valor do titulo
	 * @return
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * Método de modificação do valor do Titulo
	 * @param titulo
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * Método de acesso para o retorno do valor da descrição
	 * @return
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * Método de modificação do valor da descrição
	 * @param descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * Método de acesso para o retorno do link de diretorio
	 * @return
	 */
	public String getDiretorio() {
		return diretorio;
	}
	/**
	 * Método de modificação do link de diretorio
	 * @param diretorio
	 */
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}
	/**
	 * Método de acesso para o retorno do valor da mensagem para o Adm
	 * @return
	 */
	public String getMensagem() {
		return mensagem;
	}
	/**
	 * Método de modificação do valor da mensagem
	 * @param mensagem
	 */
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	/**
	 * Método de inserção de Informaçoes na tabela Novidade
	 * @return
	 * @throws IOException
	 */
	public String adicionarNovidade() throws IOException {
		
		try(InputStream entrada= imagem.getInputStream()) {
			String pasta= "/home/lucas/eclipse-workspace/BarberShop/WebContent/resources/imagens/";
			String nome= imagem.getSubmittedFileName();
			Files.copy(entrada, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
			String a= pasta.substring(51,70);
			String caminho= ""+a+""+nome+"";
			Date dAtual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String dataAtual = sdf.format(dAtual);
			this.setData(dataAtual);
			Novidade novi= new Novidade(this.id,this.getTitulo(),caminho,this.getDescricao(),this.getData());
			if(new NovidadeController().inserir(novi)) {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			    String url = ec.getRequestContextPath() + "/faces/home.xhtml";
			    ec.redirect(url);			
			}else {				
				this.setMensagem("<script>UIkit.modal.alert('Novidade não Adicionada!').then(function() {\n" + 
						"    console.log('Confirmed.')\n" + 
						"});</script>");
				return null;
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	/**
	 * Método para o preenchimento de dados na pagina de Edição de Novidades
	 * @param id
	 */
	public void carregarId(int id) {
		Novidade novi= new NovidadeController().selecionarIdN(id);
		this.setId(novi.getId());
		this.setTitulo(novi.getTitulo());
		this.setDescricao(novi.getDescricao());
		this.setDiretorio(novi.getImagem());
	}
	/**
	 * Método para a alteração das informações especificas de um bloco da tabela Novidades
	 * @return
	 */
	public String editar() {
		try(InputStream entrada= imagem.getInputStream()) {
			String pasta= "/home/lucas/eclipse-workspace/BarberShop/WebContent/resources/imagens/";
			String nome= imagem.getSubmittedFileName();
			Files.copy(entrada, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
			String a= pasta.substring(51,70);
			String caminho= ""+a+""+nome+"";
			Date dAtual = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			String dataAtual = sdf.format(dAtual);
			this.setData(dataAtual);
			Novidade novi= new Novidade(this.getId(),this.getTitulo(),caminho,this.getDescricao(),this.getData());
			if(new NovidadeController().editar(novi)) {
				ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			    String url = ec.getRequestContextPath() + "/faces/home.xhtml";
			    ec.redirect(url);			
			}else {				
				this.setMensagem("<script>UIkit.modal.alert('Novidade não Adicionada!').then(function() {\n" + 
						"    console.log('Confirmed.')\n" + 
						"});</script>");
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		return null;
		
	}
	/**
	 * Método para deletar informações especificas da tabela Novidade
	 * @param id
	 * @return
	 */
	public String deletar(int id) {
		new NovidadeController().remover(id);
		return "home.xhtml?faces-redirect=true;";
	}

}
