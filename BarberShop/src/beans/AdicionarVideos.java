package beans;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Part;

import controle.TatuagensController;
import controle.VideosController;
import controle.VideosController;
import modelo.Tatuagens;
import modelo.Videos;
import modelo.Videos;
/**
 * 
 * @author lucas
 *
 */
@ManagedBean(name="AdicionarVideos")

public class AdicionarVideos {
		private int id;
		private String titulo,descricao,diretorio,data;
		private Part video;

		/**
		 * Método de acesso para o retorno do valor da data
		 * @return
		 */
		public String getData() {
			return data;
		}
		/**
		 * Método de modificação do valor da data
		 * @param data
		 */
		public void setData(String data) {
			this.data = data;
		}
		/**
		 * Método de acesso para o retorno do valor do titulo
		 * @return
		 */
		public String getTitulo() {
			return titulo;
		}
		/**
		 * Método de modificação do valor do Titulo
		 * @param titulo
		 */
		public void setTitulo(String titulo) {
			this.titulo = titulo;
		}
		/**
		 * Método de acesso para o retorno do valor da descrição
		 * @return
		 */
		public String getDescricao() {
			return descricao;
		}
		/**
		 * Método de modificação do valor da descrição
		 * @param descricao
		 */
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
		/**
		 * Método de acesso para o retorno do valor do id
		 * @return
		 */
		public int getId() {
			return id;
		}
		/**
		 * Método de modificação do valor do id
		 * @param id
		 */
		public void setId(int id) {
			this.id = id;
		}
		/**
		 * Método de acesso para o retorno do link de diretorio
		 * @return
		 */
		public String getDiretorio() {
			return diretorio;
		}
		/**
		 * Método de modificação do link de diretorio
		 * @param diretorio
		 */
		public void setDiretorio(String diretorio) {
			this.diretorio = diretorio;
		}
		/**
		 * Método de acesso para o retorno do Video
		 * @return
		 */
		public Part getVideo() {
			return video;
		}
		/**
		 * Método de modificação do valor do Video
		 * @param imagem
		 */
		public void setVideo(Part video) {
			this.video = video;
		}
		/**
		 * Método de Inserção de informações na tabela Videos
		 * @return
		 * @throws IOException
		 */
		public String adicionarVideo() throws IOException {
			try(InputStream entrada= video.getInputStream()) {
				String pasta= "/home/lucas/eclipse-workspace/BarberShop/WebContent/resources/imagens/";
				String nome= video.getSubmittedFileName();
				Files.copy(entrada, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
				String a= pasta.substring(51,70);
				String caminho= ""+a+""+nome+"";
				Date dAtual = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				String dataAtual = sdf.format(dAtual);
				this.setData(dataAtual);
				Videos vi = new Videos(this.getId(),caminho,this.getTitulo(),this.getDescricao(),this.getData());
				if(new VideosController().inserir(vi)) {
					ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
				    String url = ec.getRequestContextPath() + "/faces/home.xhtml";
				    ec.redirect(url);
				}else {
					return "index.xhtml?faces-redirect=true;";
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		/**
		 * Array das informações contidas na tabela Videos
		 */
		ArrayList<Videos> lista = new VideosController().consultarVideos();
		public ArrayList <Videos> getLista() {
			return lista;
		}
		public void setLista(ArrayList <Videos> lista) {
			this.lista = lista;
		}
		/**
		 * Array das ultimas informações contidas na tabela Videos
		 */
		ArrayList<Videos> listau = new VideosController().consultarUltimosVideos();
		public ArrayList <Videos> getListau() {
			return listau;
		}
		public void setListau(ArrayList <Videos> listau) {
			this.listau = listau;
		}
		/**
		 * Método para o preenchimento de dados na pagina de Edição de Videos
		 * @param id
		 */
		public void carregarId(int id) {
			Videos vi = new VideosController().selecionarIdV(id);
			this.setId(vi.getId());
			this.setDiretorio(vi.getVideo());
			this.setTitulo(vi.getTitulo());
			this.setDescricao(vi.getDescricao());
		}
		/**
		 * Método para a alteração das informações especificas de um bloco da tabela Videos
		 * @return
		 */
		public String editar() {
			try(InputStream entrada= video.getInputStream()) {
				String pasta= "/home/lucas/eclipse-workspace/BarberShop/WebContent/resources/imagens/";
				String nome= video.getSubmittedFileName();
				Files.copy(entrada, new File(pasta,nome).toPath(), StandardCopyOption.REPLACE_EXISTING);
				String a= pasta.substring(51,70);
				String caminho= ""+a+""+nome+"";
				Date dAtual = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				String dataAtual = sdf.format(dAtual);
				this.setData(dataAtual);
				Videos vi= new Videos(this.getId(),caminho,this.getTitulo(),this.getDescricao(),this.getData());
				if(new VideosController().editar(vi)) {
					return "home.xhtml?faces-redirect=true;";	
				}else {
					return "index.xhtml?faces-redirect=true;";
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			return null;
			
		}
		/**
		 * Método para deletar informações especificas da tabela Videos
		 * @param id
		 * @return
		 */
		public String deletar(int id) {
			new VideosController().remover(id);
			return "home.xhtml?faces-redirect=true;";
		}
		
		
	}
		
