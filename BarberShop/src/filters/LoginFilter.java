package filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.verificarUser;
/**
 * Filtragem para a criação de Sessão e de redirecionamento de páginas
 * @author lucas
 *
 */
public class LoginFilter implements Filter {

	@Override
	/**
	 * Método de verificação e criação de Sessão e Endereço de paginas
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req= (HttpServletRequest) request;
		HttpServletResponse res= (HttpServletResponse) response;
		verificarUser session=(verificarUser) req.getSession().getAttribute("verificarUser");
		String url= req.getRequestURI();
		if(session==null || !session.logado) {
			if(url.indexOf("home.xhtml")>=0 || url.indexOf("logout.xhtml")>=0) {
				res.sendRedirect(req.getServletContext().getContextPath()+ "/faces/login.xhtml");
			}else {
				chain.doFilter(request, response);
			}
		}else{
			if(url.indexOf("login.xhtml")>=0) {
				res.sendRedirect(req.getServletContext().getContextPath()+ "/faces/home.xhtml");
			}
			else if(url.indexOf("logout.xhtml")>=0){
				req.getSession().removeAttribute("verificarUser");
				res.sendRedirect(req.getServletContext().getContextPath()+ "/faces/login.xhtml");
			}else {
				chain.doFilter(request, response);
			}
		}
	}

}
