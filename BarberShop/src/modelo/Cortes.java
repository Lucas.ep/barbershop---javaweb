package modelo;
import java.io.InputStream;
/**
 * Modelo Cortes
 * @author lucas
 *
 */
public class Cortes {
	private int id;
	private String nome;
	private String descricao;
	private String imagem;
	private float preco;
	private String data;
	/**
	 * Construtor da modelo Cortes para referenciar o uso da mesma com outros métodos nas Beans
	 * @param id
	 * @param nome
	 * @param descricao
	 * @param preco
	 * @param imagem
	 * @param data
	 */
	public Cortes(int id,String nome, String descricao,float preco,String imagem,String data) {
		this.setId(id);
		this.setNome(nome);
		this.setDescricao(descricao);
		this.setPreco(preco);
		this.setImagem(imagem);
		this.setData(data);
	}
	
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

	public float getPreco() {
		return preco;
	}
	public void setPreco(float preco) {
		this.preco = preco;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
	
	
	
}
