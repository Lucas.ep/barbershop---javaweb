package modelo;
import java.io.InputStream;
/**
 * Modelo Produto
 * @author lucas
 *
 */
public class Produtos {
	private int id;
	private String nome;
	private String descricao;
	private float preco;
	private String imagem;
	private String data;
	/**
	 * Construtor da modelo Produtos para referenciar o uso da mesma com outros métodos nas Beans
	 * @param id
	 * @param nome
	 * @param descricao
	 * @param preco
	 * @param imagem
	 * @param data
	 */
	public Produtos(int id,String nome, String descricao,float preco,String imagem,String data) {
		this.setId(id);
		this.setNome(nome);
		this.setDescricao(descricao);
		this.setPreco(preco);
		this.setImagem(imagem);
		this.setData(data);
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public float getPreco() {
		return preco;
	}

	public void setPreco(float preco) {
		this.preco = preco;
	}

	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
		
}
