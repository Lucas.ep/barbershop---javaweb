package modelo;

import java.io.InputStream;
/**
 * Modelo Videos
 * @author lucas
 *
 */
public class Videos {
	private int id;
	private String video,titulo,descricao,data;
	/**
	 * Construtor da modelo Videos para referenciar o uso da mesma com outros métodos nas Beans
	 * @param id
	 * @param video
	 * @param titulo
	 * @param descricao
	 * @param data
	 */
	public Videos(int id,String video,String titulo,String descricao,String data) {
		this.setId(id);
		this.setVideo(video);
		this.setTitulo(titulo);
		this.setDescricao(descricao);
		this.setData(data);
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}
	
	
	
}
