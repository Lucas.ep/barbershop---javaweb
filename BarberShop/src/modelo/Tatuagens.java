package modelo;
/**
 * Modelo Tatuagens
 * @author lucas
 *
 */
public class Tatuagens {
	private int id;
	private String imagem;
	private String data;
	/**
	 * Construtor da modelo Tatuagens para referenciar o uso da mesma com outros métodos nas Beans
	 * @param id
	 * @param imagem
	 * @param data
	 */
	public Tatuagens(int id,String imagem,String data) {
		this.setId(id);
		this.setImagem(imagem);
		this.setData(data);
	}
	
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
	
	
}
