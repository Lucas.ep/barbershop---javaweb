package modelo;
/**
 * Modelo Usuario
 * @author lucas
 *
 */
public class Usuario {
	private int id;
	private String nome;
	private String email;
	private String senha;
	/**
	 * Construtor da modelo Usuario para referenciar o uso da mesma com outros métodos nas Beans
	 * @param nome
	 * @param email
	 * @param senha
	 */
	public Usuario(String nome,String email, String senha) {
		this.setNome(nome);
		this.setEmail(email);
		this.setSenha(senha);
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}	
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
