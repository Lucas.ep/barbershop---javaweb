package modelo;
import java.io.InputStream;
import java.sql.Date;

import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.Part;

import com.mysql.jdbc.Blob;
/**
 * Modelo Novidade
 * @author lucas
 *
 */
public class Novidade {
	private int id;
	private String imagem;
	private String titulo;
	private String descricao;
	private String data;
	/**
	 * Construtor da modelo Novidade para referenciar o uso da mesma com outros métodos nas Beans
	 * @param id
	 * @param titulo
	 * @param imagem
	 * @param descricao
	 * @param data
	 */
	public Novidade(int id,String titulo,String imagem, String descricao,String data) {
		this.setId(id);
		this.setImagem(imagem);
		this.setTitulo(titulo);
		this.setDescricao(descricao);
		this.setData(data);
	}

	public String getData() {
		return data;
	}
	
	public void setData(String data) {
		this.data = data;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
	
}
